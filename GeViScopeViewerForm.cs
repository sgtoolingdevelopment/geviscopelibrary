﻿using System.Drawing;
using System.Windows.Forms;

namespace GeViScopeLibrary
{
    public partial class GeViScopeViewerForm : Form
    {
        public GeViScopeViewerForm()
        {
            InitializeComponent();
        }

        public void SetSnapshot(Bitmap snapshot)
        {
            pbSnapshot.Image = snapshot;
            SetClientSizeCore(snapshot.Width + (pbSnapshot.Left * 2) + 2, snapshot.Height + (pbSnapshot.Top * 2) + 2);
        }
    }
}
