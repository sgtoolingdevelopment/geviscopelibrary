﻿using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("SNAPSHOT LIBRARY")]
[assembly: AssemblyDescription("All rights reserved. This software is the property of Framatome Inc. and is to be considered proprietary and confidential and is protected by copyright law and international treaties. This software may not be reproduced or copied in whole or in part, nor may it be furnished to others without the express written consent and permission of Framatome Inc, nor may it be used in any way that is or may be detrimental or injurious to Framatome Inc. This software and any copies that may have been made must be returned to Framatome Inc. upon request.")]
[assembly: AssemblyConfiguration("2019/09/05")]
[assembly: AssemblyCompany("Framatome Inc.")]
[assembly: AssemblyProduct("122-9300381-000")]
[assembly: AssemblyCopyright("Copyright © 2018-2019 Framatome Inc.")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("f49b8e3e-e3ac-4e2d-8abb-4c45d5fbb585")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("0.2019.9.5")]
[assembly: AssemblyFileVersion("0.2019.9.5")]
[assembly: NeutralResourcesLanguage("en-US")]
